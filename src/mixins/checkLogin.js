export default {
  mounted () {
    // eslint-disable-next-line
    var cookieUser = document.cookie.replace(/(?:(?:^|.*;\s*)dataUser\s*\=\s*([^;]*).*$)|^.*$/, '$1')
    // cek cookie jika di halaman login
    if (this.$router.currentRoute.name === 'login') {
      if (cookieUser) {
        this.$router.push('/')
      } else {
        return true
      }
    }
    // cek jika cookie ada, di halaman selain login
    if (cookieUser) {
      return true
    } else {
      this.$router.push('/login')
    }
  }
}
