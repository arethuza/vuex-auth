import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from './router'

Vue.use(Vuex)

const login = {
  namespaced: true,
  state: {
    data: {
      errors: null,
      wrongPass: null
    }
  },
  mutations: {
    saveError (state, payload) {
      state.data.errors = payload
    },
    wrongPass (state, payload) {
      state.data.wrongPass = payload
    }
  },
  actions: {
    login ({ commit }, payload) {
      axios.post('http://10.30.30.126:8000/api/login', {
        email: payload.email,
        password: payload.password
      }).then(response => {
        document.cookie = `dataUser=${response.data.data.token}`
        const dataUser = JSON.stringify(response.data)
        localStorage.setItem('dataUser', dataUser)
        router.push('/')
      }).catch(function (error) {
        if (error.response.status === 422) {
          if (error.response.data.errors) {
            commit('saveError', error.response.data.errors)
          } else {
            commit('wrongPass', error.response.data.message)
          }
        } else {
          alert('koneksi terputus')
        }
      })
    },
    logout () {
      document.cookie = 'dataUser='
      router.push('/login')
    }
  }

}
export default new Vuex.Store({
  modules: {
    login: login
  }
})
